﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static Dictionary<string, string> _user = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("123", "123");
        }

        private void button1_Click(object sender, EventArgs e)
        {

            var uid = userName.Text;
            var pwd = passWord.Text;

            var res = _user.Where(x => x.Key == uid).FirstOrDefault();

            if (res.Key==uid && res.Value== pwd)
            {
                MessageBox.Show("登陆成功","恭喜你",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            }
            else 
            {
                MessageBox.Show("登陆失败,，你需要注册账号","很遗憾",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RegisterForm registerForm = new RegisterForm();
            registerForm.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Enroll : Form
    {
        public Enroll()
        {
            InitializeComponent();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = enrolluid.Text;
            var pwd = enrollpsw.Text;
            var reps = repsw.Text;

            var isUidNull = string.IsNullOrEmpty(uid);
            var isPwdNull = string.IsNullOrEmpty(pwd);
            var isEqure = pwd == reps;
            if (!isUidNull && !isPwdNull && isEqure)
            {
                 MessageBox.Show("注册成功","注册结果框",MessageBoxButtons.OK,MessageBoxIcon.Information);
                Login._user.Add(uid,pwd);
            }
            else
            {
                MessageBox.Show("注册失败","注册结果框",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

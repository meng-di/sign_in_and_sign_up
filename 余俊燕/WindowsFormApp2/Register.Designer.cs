﻿namespace WindowsFormApp2
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.passWord = new System.Windows.Forms.TextBox();
            this.userName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCancelRegister = new System.Windows.Forms.Button();
            this.btnRegiter = new System.Windows.Forms.Button();
            this.respassWord = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // passWord
            // 
            this.passWord.Location = new System.Drawing.Point(319, 177);
            this.passWord.Name = "passWord";
            this.passWord.Size = new System.Drawing.Size(149, 21);
            this.passWord.TabIndex = 7;
            // 
            // userName
            // 
            this.userName.Location = new System.Drawing.Point(319, 110);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(149, 21);
            this.userName.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(240, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "密码";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(240, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "用户名";
            // 
            // btnCancelRegister
            // 
            this.btnCancelRegister.Location = new System.Drawing.Point(377, 311);
            this.btnCancelRegister.Name = "btnCancelRegister";
            this.btnCancelRegister.Size = new System.Drawing.Size(62, 23);
            this.btnCancelRegister.TabIndex = 9;
            this.btnCancelRegister.Text = "取消注册";
            this.btnCancelRegister.UseVisualStyleBackColor = true;
            // 
            // btnRegiter
            // 
            this.btnRegiter.Location = new System.Drawing.Point(242, 311);
            this.btnRegiter.Name = "btnRegiter";
            this.btnRegiter.Size = new System.Drawing.Size(62, 23);
            this.btnRegiter.TabIndex = 8;
            this.btnRegiter.Text = "注册";
            this.btnRegiter.UseVisualStyleBackColor = true;
            this.btnRegiter.Click += new System.EventHandler(this.btnRegiter_Click);
            // 
            // respassWord
            // 
            this.respassWord.Location = new System.Drawing.Point(319, 242);
            this.respassWord.Name = "respassWord";
            this.respassWord.Size = new System.Drawing.Size(149, 21);
            this.respassWord.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(240, 251);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "确认密码";
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.respassWord);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancelRegister);
            this.Controls.Add(this.btnRegiter);
            this.Controls.Add(this.passWord);
            this.Controls.Add(this.userName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Name = "Register";
            this.Text = "Register";
            this.Load += new System.EventHandler(this.Register_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox passWord;
        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCancelRegister;
        private System.Windows.Forms.Button btnRegiter;
        private System.Windows.Forms.TextBox respassWord;
        private System.Windows.Forms.Label label1;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            var uId = uName.Text;
            var pwd = uWord.Text;
            var cPwd = erWord.Text;

            var isUidNull = string.IsNullOrEmpty(uId);
            var isPwdNull = string.IsNullOrEmpty(pwd);
            var isEqure = pwd == cPwd;

            if (!isUidNull && !isPwdNull && isEqure)
            {
                MessageBox.Show("注册成功", "恭喜！", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                Form1._user.Add(uId, pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败", "请重新注册！", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
